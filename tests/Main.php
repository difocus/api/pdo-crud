<?php
namespace ShopExpress\PdoCrud\Test;

use DateTime;

final class Main
{
    private function __construct()
    {
    }

    public static function dateTimeToStr(DateTime $date)
    {
        return $date->format('Y-m-d H:i:s');
    }

    public static function shopCreatedDateTimeString($createdDate, $createdTime)
    {
        return date('Y-m-d H:i:s', strtotime($createdDate . ' ' . $createdTime));
    }

    public static function makeRequest($objectUrl, $method, $data = [])
    {
        $ch = curl_init();

        $opts = [
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_USERAGENT => 'dicms-api-php-beta-0.2',
        ];
        $opts[CURLOPT_URL] = $objectUrl;

        switch ($method) {
            case 'GET':
            case 'PUT':
            case 'POST':
            case 'PATCH':
            case 'DELETE':
                $postdata = http_build_query($data);
                $opts[CURLOPT_CUSTOMREQUEST] = $method;
                $opts[CURLOPT_RETURNTRANSFER] = true;
                $opts[CURLOPT_POSTFIELDS] = $postdata;
                $opts[CURLOPT_HTTPHEADER][] = 'Content-Length: ' . strlen($postdata);
                break;
        }

        curl_setopt_array($ch, $opts);
        $resultBody = curl_exec($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $errno = curl_errno($ch);
        $error = curl_error($ch);

        curl_close($ch);

        if ($resultBody === false) {
            throw new \Exception(
                sprintf("CURL Error %s: %s", $errno, $error)
            );
        }

        return json_decode($resultBody, true);
    }
}
