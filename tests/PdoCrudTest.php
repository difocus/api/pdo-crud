<?php
namespace ShopExpress\PdoCrud\Test;

use Exception;
use PHPUnit\Framework\TestCase;
use ShopExpress\PdoCrud\Test\Main as MainHelper;

class PdoCrudTest extends TestCase
{
    protected static $config;
    protected static $table;

    public static function setUpBeforeClass()
    {
        if (!file_exists(__DIR__ . '/../.env')) {
            self::fail('File .env does not exists');
        }
        $config = parse_ini_file(__DIR__ . '/../.env');

        static::$config = [
            'url' => $config['BASE_URL'],
            'table' => 'shops',
            'token' => $config['API_TOKEN'],
            'identity' => 5,
        ];
        static::$table = 'shops_' . uniqid();
    }

    /**
     * @throws Exception
     * @return mixed
     */
    public function testCreate()
    {
        $response = MainHelper::makeRequest(
            static::$config['url'] . '/' . static::$table . '?token=' . static::$config['token'],
            'PUT',
            [
                'id' => static::$config['identity'],
                'shop_id' => '1323',
                'oid' => '1212',
                'name' => 'Информация по делу о пропаже ёжика',
                'title' => 'Информация по делу о пропаже ёжика',
                'content' => 'Клиентский спрос индуктивно синхронизирует медиамикс, повышая конкуренцию. Направленный маркетинг отталкивает общественный отраслевой стандарт. Косвенная реклама консолидирует институциональный опрос. Как отмечает Майкл Мескон, восприятие марки конструктивно. Баннерная реклама без оглядки на авторитеты неоднозначна. Создание приверженного покупателя, не меняя концепции, изложенной выше, традиционно синхронизирует медиамикс. Реклама переворачивает комплексный рекламный блок. Привлечение аудитории амбивалентно. Еще Траут показал, что изменение глобальной стратегии многопланово стабилизирует из ряда вон выходящий принцип восприятия.'
            ]
        );
        $message = isset($response['error']['message']) ? $response['error']['message'] : '';
        $this->assertTrue(isset($response['success']), $message);

        return static::$config['identity'];
    }

    /**
     * @depends testCreate
     * @throws Exception
     */
    public function testRead($id)
    {
        $response = MainHelper::makeRequest(
            static::$config['url'] . '/' . static::$table . '/' . $id . '?token=' . static::$config['token'],
            'GET',
            [
                'SELECT' => [
                    'id',
                    "SNIPPET(name, 'дело | информация') as some_name",
                ]
            ]
        );

        $message = isset($response['error']['message']) ? $response['error']['message'] : '';

        if (isset($response[0]['id'])) {
            $this->assertEquals($response[0]['id'], $id);

            $this->assertTrue(isset($response[0]['some_name']), $message);

            $this->assertEquals($response[0]['some_name'], '<b>Информация</b> по <b>делу</b> о пропаже ёжика', $message);
        } else {
            $this->fail('Record was not received! ' . $message);
        }

        return $response[0]['id'];
    }

    /**
     * @depends testCreate
     * @throws Exception
     */
    public function testMatchByName($id)
    {
        $response = MainHelper::makeRequest(
            static::$config['url'] . '/' . static::$table . '/match/ёжик?token=' . static::$config['token'],
            'GET'
        );

        if (isset($response[0]['id'])) {
            $this->assertEquals($response[0]['id'], $id);
        } else {
            $this->fail('Record was not finded!');
        }
    }

    /**
     * @depends testCreate
     * @throws Exception
     */
    public function testMatchByContent($id)
    {
        $response = MainHelper::makeRequest(
            static::$config['url'] . '/' . static::$table . '/match/спрос индуктивно синхронизирует медиамикс?token=' . static::$config['token'],
            'GET'
        );

        if (isset($response[0]['id'])) {
            $this->assertEquals($response[0]['id'], $id);
        } else {
            $this->fail('Record was not finded!');
        }
    }

    /**
     * @depends testCreate
     * @throws Exception
     */
    public function testRaw($id)
    {
        $response = MainHelper::makeRequest(
            static::$config['url'] . '/raw?token=' . static::$config['token'],
            'GET',
            [
                'RAW' => 'SELECT id FROM ' . static::$table . ' WHERE oid = 1212',
            ]
        );

        $message = isset($response['error']['message']) ? $response['error']['message'] : '';

        if (isset($response[0]['id'])) {
            $this->assertEquals($response[0]['id'], $id, $message);
        } else {
            $this->fail('Record was not finded! ' . $message);
        }
    }

    /**
     * @depends testRead
     * @throws Exception
     */
    public function testUpdate($id)
    {
        $response = MainHelper::makeRequest(
            static::$config['url'] . '/' . static::$table . '/' . $id . '?token=' . static::$config['token'],
            'PATCH',
            ['name' => 'Shop Test 2']
        );

        $message = isset($response['error']['message']) ? $response['error']['message'] : '';

        $response = MainHelper::makeRequest(
            static::$config['url'] . '/' . static::$table . '/' . $id . '?token=' . static::$config['token'],
            'GET'
        );
        if (isset($response[0]['name'])) {
            $this->assertEquals($response[0]['name'], 'Shop Test 2', $message);
        } else {
            $this->fail('Record was not received! ' . $message);
        }

        return $response[0]['id'];
    }

    /**
     * @depends testCreate
     * @throws Exception
     */
    public function testDelete($id)
    {
        $response = MainHelper::makeRequest(
            static::$config['url'] . '/' . static::$table . '/' . $id . '?token=' . static::$config['token'],
            'DELETE'
        );

        $this->assertTrue(isset($response['success']));
    }

    public static function tearDownAfterClass()
    {
        static::$config = null;
    }
}
