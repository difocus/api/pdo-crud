# Description for PDO CRUD

## Getting Started

### Create table

```
Request(
	'http://yoursite.com/createtable?token=<your_token>',
	'POST',
	[
            'name' => 'tablename',
            'fields' => [
                'name field stored',
                'oid integer',
                'shop_id integer'
            ]
	]
);
```

### Create/Replace

```
Request(
	'http://yoursite.com/shops?token=<your_token>',
	'POST' or 'PUT',
	[
	    'id' => 1,
	    'shop_id' => '1323',
	    'oid' => '1212',
	    'name' => 'Информация по делу о пропаже ёжика'
	]
);
```

### Read by id

```
Request(
	'http://yoursite.com/shops/1?token=<your_token>',
	'GET',
	[
	    'select' => [
	        'id',
	        "SNIPPET(name, 'дело | информация') as some_name",
	    ]
	]
);
```

### Read by attributes

```
Request(
	'http://yoursite.com/shops?token=<your_token>',
	'GET',
	[
	    'select' => [
	        'id',
	        "SNIPPET(name, 'дело | информация') as some_name",
	    ],
        'where' => [
            'oid' => 5,
            'shop_id' => 6
        ]
	]
);

Request(
	'http://yoursite.com/shops?token=<your_token>',
	'GET',
	[
	    'select' => [
	        'id',
	        "SNIPPET(name, 'дело | информация') as some_name",
	    ],
        'where' => [
        	'attrobutes' => [
				'oid',
				'shop_id'
        	],
        	'values' => [
				5,
				1
        	],
        	'operators' => [
				'!=',
				'='
        	]
        ]
	]
);
```

### Match

```
Request(
	'http://yoursite.com/shops/match/ёжик?token=<your_token>',
	'GET'
);
```

### Update

```
Request(
	'http://yoursite.com/shops/1?token=<your_token>',
	'PATCH',
        [
            'oid' => '1234',
            'sid' => 'text value'
        ]
);
```

### Delete by id

```
Request(
	'http://yoursite.com/shops/1?token=<your_token>',
	'DELETE'
);
```

### Delete by attributes

```
Request(
	'http://yoursite.com/shops?token=<your_token>',
	'DELETE'
        [
            'oid' => 10,
            'sid' => 'text value'
        ]
);
```

### Raw Query

```
Request(
	'http://yoursite.com/raw?token=<your_token>',
	'GET',
	[
		'raw' => 'FLUSH RTINDEX shops',
        'type' => < *'fetchAll' | 'fetch' | 'rowCount' >
	]
);
```

### Database Map

```
Request(
	'http://yoursite.com/database?token=<your_token>',
	'GET'
);
```

## Authors

* **Evgeniy Petrov** - *Initial work* - [MrKody](https://github.com/MrKody)