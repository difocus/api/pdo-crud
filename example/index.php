<?php
require 'vendor/autoload.php';

use ShopExpress\PdoCrud\Factory\StorageFactory;
use ShopExpress\PdoCrud\Factory\StorageFactoryConfiguration;
use ShopExpress\PdoCrud\RemoteControl;

try {
    if (!file_exists(__DIR__.'/.env')) {
        throw new Exception('File .env does not exists', 500);
    }
    $config = parse_ini_file(__DIR__.'/.env');

    if (@$_GET['token'] == $config['API_TOKEN']) {
        $configuration = new StorageFactoryConfiguration((int)$config['DB_DRIVER']);
        $configuration->setHost($config['DB_HOST']);
        $configuration->setPort((int)$config['DB_PORT']);
        $configuration->setDb($config['DB_DATABASE']);
        $configuration->setLogin($config['DB_USERNAME']);
        $configuration->setPassword($config['DB_PASSWORD']);

        $storageFactory = new StorageFactory($configuration);
        $storage = $storageFactory->create();

        $api = new RemoteControl($storage, $config['BASE_URL']);
        die(json_encode($api->rest()->getAnswer()));
    } else {
        throw new Exception('Invalid API token!', 401);
    }
} catch (Throwable $e) {
    $error = array(
        'error' => array(
            'message' => $e->getMessage(),
            'code'    => $e->getCode(),
        ),
    );
    header('Content-type: application/json');
    die(json_encode($error));
}
