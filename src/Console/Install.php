<?php
namespace ShopExpress\PdoCrud\Console;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;
use ShopExpress\PdoCrud\Factory\StorageFactoryConfiguration;

class Install
{
    /**
     * @param Event $event
     */
    public static function postInstall(Event $event)
    {
        $config = '';
        $io = $event->getIO();
        $arg = $io->ask('Database host (default - 127.0.0.1): ');
        $config .= 'DB_HOST=' . (escapeshellcmd($arg)? escapeshellcmd($arg): '127.0.0.1');
        $config .= "\n";

        $arg = $io->ask('Database port (mysql - 3306, postgre - 5432, sphinx - 9306): ');
        $config .= 'DB_PORT=' . (escapeshellcmd($arg)? escapeshellcmd($arg): '3306');
        $config .= "\n";

        $arg = $io->select('Database driver (default - mysql): ', StorageFactoryConfiguration::$drivers, 0);
        $config .= 'DB_DRIVER=' . escapeshellcmd($arg);
        $config .= "\n";

        $arg = $io->ask('Database name: ');
        $config .= 'DB_DATABASE=' . escapeshellcmd($arg);
        $config .= "\n";

        $arg = $io->ask('Database login: ');
        $config .= 'DB_USERNAME=' . escapeshellcmd($arg);
        $config .= "\n";

        $arg = $io->askAndHideAnswer('Database password: ');
        $config .= 'DB_PASSWORD=' . escapeshellcmd($arg);
        $config .= "\n";
        $config .= "\n";

        $arg = $io->ask('Enter base url (e.g. /var/www/site.ru): ');
        $config .= 'BASE_URL=' . escapeshellcmd($arg);
        $config .= "\n";

        $arg = $io->ask('Enter api token (or leave empty for generate): ');
        $config .= 'API_TOKEN=' . (escapeshellcmd($arg)? escapeshellcmd($arg): md5(microtime(true)));
        $config .= "\n";

        file_put_contents('.env', $config);

        $arg = $io->askConfirmation('Copy "example/index.php" file to root directory (yes/no, default - yes)? ', true);
        if (escapeshellcmd($arg)) {
            try {
                copy('example/index.php', 'index.php');
            } catch (IOException $e) {
                throw new \InvalidArgumentException('<error>Could not copy "example/index.php"</error>');
            }
        } else {
            $io->write('File "example/index.php" wasn\'t copied');
        }

        $io->write('PDO-CRUD Successfully installed!');
    }
}
