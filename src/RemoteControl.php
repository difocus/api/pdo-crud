<?php

namespace ShopExpress\PdoCrud;

use Exception;
use ShopExpress\PdoCrud\Storage\StorageInterface;
use ShopExpress\RequestResponse\Exception\InvalidRequestException;
use ShopExpress\RequestResponse\Request\Request;

class RemoteControl
{
    /**
     * @var StorageInterface
     */
    private $storage;
    private $segments;
    private $mapDatabase;
    private $answer;

    /**
     * @var Request
     */
    private $request;

    /**
     * RemoteControl constructor.
     * @param StorageInterface $storage
     * @param string $base_uri
     * @throws InvalidRequestException
     */
    public function __construct(StorageInterface $storage, $base_uri = '')
    {
        $this->storage = $storage;
        $this->request = $this->request instanceof Request ? $this->request : Request::createFromGlobals();
        $this->mapDatabase = $this->storage->mapDatabase();
        $this->segments = $this->get_uri_segments($base_uri);
    }

    /**
     * Handle the REST calls and map them to corresponding CRUD
     *
     * @access public
     * @throws Exception
     */
    public function rest()
    {
        header('Content-type: application/json');
        /*
        create > POST   /table
        read   > GET    /table[/id]
        update > PATCH  /table/id
        replace > PUT   /table[/id]
        delete > DELETE /table/id
        */
        switch ($this->request->getMethod()) {
            case 'POST':
                $this->create();
                break;
            case 'GET':
                $this->read();
                break;
            case 'PATCH':
                $this->update();
                break;
            case 'PUT':
                $this->replace();
                break;
            case 'DELETE':
                $this->delete();
                break;
        }
        return $this;
    }

    /**
     * Get the URI segments from the URL
     *
     * @param string $base_uri Optional base URI if not in root folder
     * @return array Returns array of URI segments
     * @access private
     */
    private function get_uri_segments($base_uri)
    {
        // Fix REQUEST_URI if required
        if (!isset($_SERVER['REQUEST_URI'])) {
            $_SERVER['REQUEST_URI'] = substr($_SERVER['PHP_SELF'], 1);
            if (isset($_SERVER['QUERY_STRING'])) {
                $_SERVER['REQUEST_URI'] .= '?'. $_SERVER['QUERY_STRING'];
            }
        }

        $url = '';
        $request_url = $_SERVER['REQUEST_URI'];
        $script_url  = $_SERVER['SCRIPT_NAME'];
        $request_url = str_replace($base_uri, '', $request_url);
        $script_url = str_replace($base_uri, '', $script_url);
        $request_url = rtrim(preg_replace('/\?.*/', '', $request_url), '/');

        if ($request_url != $script_url) {
            $url = trim(str_replace($script_url, '', $request_url), '/');
        }

        return explode('/', $url);
    }

    /**
     * Get a URI segment
     *
     * @param int $index Index of the URI segment
     * @return mixed Returns URI segment or false if none exists
     * @access private
     */
    private function segment($index)
    {
        if (isset($this->segments[$index])) {
            return rawurldecode($this->segments[$index]);
        }
        return false;
    }

    /**
     * Handles a POST and inserts into the database
     *
     * @access private
     * @throws Exception
     */
    private function create()
    {
        $table = $this->segment(0);

        if (strtoupper($table)=='CREATETABLE') {
            $this->createTable();
            return;
        }

        if (!$table || !isset($this->mapDatabase[$table])) {
            throw new Exception('Not Found', 404);
        }

        if ($data = $this->request->content()) {
            $qb = new QueryBuilder();
            $qb->insert($table, $data);
            $this->storage->query($qb);

            $this->answer = ['success' => [
                'id' => $this->storage->getLastInsertId(),
                'message' => 'Success',
                'code' => 200
            ]];
        } else {
            throw new Exception('No Content', 204);
        }
    }

    /**
     * @throws Exception
     */
    private function createTable()
    {
        $data = $this->request->content();
        if (empty($data)) {
            throw new Exception('Empty request', 204);
        }
        if (empty($data['name'])) {
            throw new Exception('Empty name', 204);
        }
        if (in_array($data['name'], $this->mapDatabase)) {
            throw new Exception('Table exist', 204);
        }
        if (!is_array($data['fields'])) {
            throw new Exception('Empty fields', 204);
        }
        $cols = [];
        if (is_array($data['fields'])) {
            foreach ($data['fields'] as $field) {
                if (!is_string($field)) {
                    throw new Exception('Error request', 204);
                }
                $cols[] = $field;
            }
        }
        $qb = new QueryBuilder();
        $qb->createTable($data['name'], $cols);
        if (!$this->storage->query($qb)) {
            throw new Exception('Error query', 204);
        }

        $this->answer = ['success' => [
            'message' => 'Success',
            'code' => 200
        ]];
    }

    /**
     * Handles a GET and reads from the database
     *
     * @access private
     * @throws Exception
     */
    private function read()
    {
        $data = $this->request->content();
        $select = $this->handlePutSelect($data);
        $where = $this->handlePutWhere($data);
        $table = $this->segment(0);

        if ($value = $this->segment(2)) {
            $field = $this->segment(1);
        } else {
            $id = intval($this->segment(1));
        }

        if ($table == 'database') {
            if (count($this->mapDatabase)) {
                return $this->answer = $this->mapDatabase;
            }

            throw new Exception('No Content', 204);
        } elseif ($table == 'raw') {
            return $this->handlePutRaw();
        }

        if (!$table || !isset($this->mapDatabase[$table])) {
            throw new Exception('Not Found', 404);
        }

        if (!empty($where)) {
            $qb = new QueryBuilder();
            $qb->select($select)
                ->from($table)
                ->order_by($this->request->order_by, $this->request->order);

            if ($this->isWhereArrayWithOperators($where)) {
                $qb->where($where['attributes'], $where['values'], $where['operators']);
            } else {
                $qb->where(array_keys($where), array_values($where));
            }

            $qb->limit((int)$this->request->limit, (int)$this->request->offset);
        } elseif (isset($id) && !empty($id) && is_int($id)) {
            $index = 'id';
            if (isset($this->mapDatabase[$table])) {
                $index = $this->mapDatabase[$table]['index'];
            }

            $qb = new QueryBuilder();
            $qb->select($select)
                ->from($table)
                ->where($index, $id);
        } elseif (!empty($field) && !empty($value)) {
            $qb = new QueryBuilder();
            $qb->select($select)
                ->from($table)
                ->where($field, $value)
                ->order_by($this->request->order_by, $this->request->order)
                ->limit((int)$this->request->limit, (int)$this->request->offset);
        } else {
            $qb = new QueryBuilder();
            $qb->select($select)
                ->from($table)
                ->order_by($this->request->order_by, $this->request->order)
                ->limit((int)$this->request->limit, (int)$this->request->offset);
        }

        $stmt = $this->storage->query($qb);
        if ($result = $stmt->fetchAll()) {
            $this->answer = $result;
        } else {
            throw new Exception('No Content', 204);
        }
    }

    /**
     * Handles a PUT select fields
     * @param array $data POST data
     * @return string fields for select expression
     */
    private function handlePutSelect($data)
    {
        $select = '*';
        if ($data) {
            if (isset($data['SELECT'])) {
                $data['select'] = $data['SELECT'];
                unset($data['SELECT']);
            }

            if (isset($data['select'])) {
                $select = [];
                foreach ($data['select'] as $field) {
                    $select[] = (string)$field;
                }
                return implode(', ', $select);
            }
        }
        return $select;
    }

    /**
     * @param array $data POST data
     * @return array
     * @throws Exception
     */
    private function handlePutWhere($data)
    {
        $where = [];
        if ($data) {
            if (isset($data['where'])) {
                if ($this->isWhereArrayWithOperators($data['where'])) {
                    foreach ($data['where']['values'] as &$value) {
                        $value = (string)$value;
                    }
                    $where = [
                        'attributes' => $data['where']['attributes'],
                        'values' => $data['where']['values'],
                        'operators' => $data['where']['operators'] ?? [],
                    ];
                } else {
                    foreach ($data['where'] as $key => $value) {
                        if (is_string($key) && is_scalar($value)) {
                            $where[$key] = (string)$value;
                        } else {
                            throw new Exception('Error request', 204);
                        }
                    }
                }
            }
        }
        return $where;
    }

    /**
     * Determines if where array with operators.
     *
     * @param array $where The where
     *
     * @return boolean True if where array with operators, False otherwise.
     */
    private function isWhereArrayWithOperators(array $where) : bool
    {
        if (isset($where['attributes'], $where['values'])
            && is_array($where['attributes']) && is_array($where['values'])
            && count($where['attributes']) == count($where['values'])
        ) {
            return true;
        }
        return false;
    }

    /**
     * @return array|mixed
     * @throws Exception
     */
    private function handlePutRaw()
    {
        if ($data = $this->request->content()) {
            if (isset($data['RAW'])) {
                $data['raw'] = $data['RAW'];
                unset($data['RAW']);
            }

            if (isset($data['raw'])) {
                $qb = new QueryBuilder();
                $qb->setSql($data['raw']);

                if (isset($data['params'])) {
                    $qb->setParams($data['params']);
                }

                if (!($stmt = $this->storage->query($qb))) {
                    throw new Exception('Query error', 404);
                }

                if (isset($data['type'])) {
                    switch ($data['type']) {
                        case 'rowCount':
                            return $this->answer = [
                                'success' => [
                                    'message' => 'Success',
                                    'rowCount' => $stmt->rowCount(),
                                    'code' => 200,
                                ],
                            ];
                        case 'fetch':
                            if ($result = $stmt->fetch()) {
                                return $this->answer = $result;
                            }

                            throw new Exception('No Content', 204);
                        case 'delete':
                        case 'update':
                        case 'insert':
                        case 'replace':
                            return $this->answer = [
                                'success' => [
                                    'message' => 'Success',
                                    'code' => 200,
                                ],
                            ];
                    }
                }

                if ($result = $stmt->fetchAll()) {
                    return $this->answer = $result;
                }

                throw new Exception('No Content', 204);
            }
        }
        throw new Exception("Empty Raw Query!");
    }

    /**
     * Handles a PATCH and updates the item
     *
     * @access private
     * @throws Exception
     */
    private function update()
    {
        $table = $this->segment(0);
        $id = intval($this->segment(1));
        $attributes = $this->request->content();

        if (!$id) {
            throw new Exception('Not Found', 404);
        }
        if (!$table || !isset($this->mapDatabase[$table])) {
            throw new Exception('Table Not Found', 404);
        }

        $index = 'id';
        if (isset($this->mapDatabase[$table])) {
            $index = $this->mapDatabase[$table]['index'];
        }

        if (empty($attributes)) {
            $rowCount = 0;
        } else {
            $qb = new QueryBuilder();
            $qb->update($table)
                ->set($attributes)
                ->where($index, $id);
            $rowCount = $this->storage->query($qb)->rowCount();
        }

        $this->answer = ['success' => [
            'message' => 'Success',
            'rowCount' => $rowCount,
            'code' => 200
        ]];
    }

    /**
     * Handles a PUT and replaces the item
     *
     * @access private
     * @throws Exception
     */
    private function replace()
    {
        $table = $this->segment(0);
        $id = intval($this->segment(1));

        if (!$data = $this->request->content()) {
            throw new Exception('No Content', 204);
        }

        if (!$table || !isset($this->mapDatabase[$table])) {
            throw new Exception('Table Not Found', 404);
        }

        $index = 'id';
        if (isset($this->mapDatabase[$table])) {
            $index = $this->mapDatabase[$table]['index'];
        }

        if (!isset($data[$index])) {
            if (!$id) {
                throw new Exception('Not Found', 404);
            } else {
                $data[$index] = $id;
            }
        }

        $qb = new QueryBuilder();
        $qb->insert($table, $data, true);
        $this->storage->query($qb);

        $this->answer = ['success' => [
            'message' => 'Success',
            'code' => 200
        ]];
    }


    /**
     * Handles a DELETE and deletes from the database
     *
     * @access private
     * @throws Exception
     */
    private function delete()
    {
        $table = $this->segment(0);
        $id = intval($this->segment(1));
        $attributes = $this->request->content();

        if (!$table || !isset($this->mapDatabase[$table])) {
            throw new Exception('Table Not Found', 404);
        }

        if (empty($id) && empty($attributes)) {
            throw new Exception('Not Found', 404);
        }

        $index = 'id';
        if (isset($this->mapDatabase[$table])) {
            $index = $this->mapDatabase[$table]['index'];
        }

        $qb = new QueryBuilder();
        $qb->delete($table);
        if (!empty($id)) {
            $qb->where($index, $id);
        } else {
            $qb->where(array_keys($attributes), array_values($attributes));
        }
        $stmt = $this->storage->query($qb);

        $this->answer = ['success' => [
            'message' => 'Success',
            'rowCount' => $stmt->rowCount(),
            'code' => 200
        ]];
    }

    /**
     * @return mixed
     */
    public function getAnswer()
    {
        return $this->answer;
    }
}