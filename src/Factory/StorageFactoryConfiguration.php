<?php
namespace ShopExpress\PdoCrud\Factory;

use Exception;
use InvalidArgumentException;

final class StorageFactoryConfiguration
{
    const ERROR_EMPTY_ARGUMENT_VALUE = 'Parameter `%s` can\'t be empty';

    private $host = '127.0.0.1';
    private $port;
    private $db;
    private $login;
    private $password;
    private $driver;

    public static $drivers = [
        'mysql',
        'postgre',
        'sphinx',
        'dummy'
    ];

    /**
     * @param integer $driver The driver identifier
     *
     * @throws Exception
     */
    public function __construct($driver = 0)
    {
        $this->setDriver($driver);
    }

    /**
     * Sets the host.
     *
     * @param string $host The host
     *
     * @throws InvalidArgumentException
     *
     * @return self
     */
    public function setHost(string $host)
    {
        if (empty($host)) {
            throw new InvalidArgumentException(sprintf(static::ERROR_EMPTY_ARGUMENT_VALUE, 'host'));
        }
        $this->host = $host;
        return $this;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * Sets the port.
     *
     * @param int $port The port
     *
     * @throws InvalidArgumentException
     *
     * @return self
     */
    public function setPort(int $port)
    {
        if (empty($port)) {
            throw new InvalidArgumentException(sprintf(static::ERROR_EMPTY_ARGUMENT_VALUE, 'port'));
        }
        $this->port = $port;
        return $this;
    }

    /**
     * Gets the port.
     *
     * @return int The port.
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * Sets the database.
     *
     * @param string $db The database
     *
     * @return self
     */
    public function setDb(string $db)
    {
        $this->db = $db;
        return $this;
    }

    /**
     * Gets the database.
     *
     * @return string The database.
     */
    public function getDb()
    {
        return $this->db;
    }

    /**
     * Sets the login.
     *
     * @param string $login The login
     *
     * @return self
     */
    public function setLogin(string $login)
    {
        $this->login = $login;
        return $this;
    }

    /**
     * Gets the login.
     *
     * @return string The login.
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Sets the password.
     *
     * @param string $password The password
     *
     * @return self
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * Gets the password.
     *
     * @return string The password.
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets the driver.
     *
     * @param int $driver The driver
     *
     * @throws Exception
     * @return self
     */
    public function setDriver(int $driver)
    {
        if (!isset(static::$drivers[$driver])) {
            throw new Exception(sprintf('Driver `%s` is not exists!', $driver));
        }
        $this->driver = $driver;
        return $this;
    }

    /**
     * Gets the driver.
     *
     * @return string The driver.
     */
    public function getDriver()
    {
        return $this->driver;
    }

    /**
     * Gets the driver name.
     *
     * @return string The driver name.
     */
    public function getDriverName()
    {
        return static::$drivers[$this->driver];
    }
}
