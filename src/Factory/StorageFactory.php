<?php
namespace ShopExpress\PdoCrud\Factory;

use ShopExpress\PdoCrud\Storage\StorageInterface;

final class StorageFactory
{
    private $configuration;

    /**
     * @param StorageFactoryConfiguration $configuration The configuration
     */
    public function __construct(StorageFactoryConfiguration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * Create an instance of the driver.
     *
     * @return StorageInterface
     */
    public function create()
    {
        $classNameStorage = "ShopExpress\\PdoCrud\\Storage\\" . mb_convert_case($this->configuration->getDriverName(), MB_CASE_TITLE) . "Storage";
        return new $classNameStorage($this->configuration);
    }
}
