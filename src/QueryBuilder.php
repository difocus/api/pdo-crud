<?php

namespace ShopExpress\PdoCrud;

use Exception;

class QueryBuilder
{
    private $sql;
    private $params = [];

    private $allowedOperators = ['=', '<', '>', '<=', '>=', '!='];

    /**
     * @param array $fields The fields
     * @param string|null $table The table
     */
    public function __construct($fields = [], $table = null)
    {
        if ($fields && $table) {
            $this->select($fields)->from($table);
        }
    }

    /**
     * @return mixed
     */
    public function getSql()
    {
        return $this->sql;
    }

    /**
     * @param $name
     * @param $cols
     *
     * @return $this
     */
    public function createTable($name, $cols)
    {
        $query = 'CREATE TABLE ' . $name . ' (' . implode(', ', $cols) . ')';

        $this->sql = $query;
        return $this;
    }

    /**
     * @param string $fields The fields
     *
     * @return self
     */
    public function select($fields = '*')
    {
        $query = "SELECT";
        if (!empty($fields) && !is_array($fields)) {
            $query .= " {$fields}";
        } elseif (is_array($fields)) {
            $query .= " `";
            $query .= implode("`,`", $fields);
            $query .= "`";
        } else {
            $query .= " *";
        }
        $this->sql = $query;
        return $this;
    }

    /**
     * Adds where the SELECT is going to be coming from (table wise).
     * select("*") select("username") select(array("username", "password"))
     *
     * @param string $table The table
     *
     * @return self
     */
    public function from($table)
    {
        $this->sql .= " FROM `{$table}`";
        return $this;
    }

    /**
     * UPDATE starter. update("users")
     *
     * @param string $table The table
     *
     * @return self
     */
    public function update($table)
    {
        $this->sql = "UPDATE `{$table}`";
        return $this;
    }

    /**
     * DELETE starter. delete("users")
     *
     * @param string $table The table
     *
     * @return self
     */
    public function delete($table)
    {
        $this->sql = "DELETE FROM `{$table}`";
        return $this;
    }

    /**
     * INSERT starter.  $data is an array matched columns to values: $data =
     * array("username" => "Caleb", "email" => "caleb@mingle-graphics.com");
     * insert("users", array("username" => "Caleb", "password" => "hash"))
     *
     * @param string $table The table
     * @param array $data The data
     * @param boolean|string $replace The replace
     *
     * @return self
     */
    public function insert($table, $data, $replace = false)
    {
        $query = ($replace ? 'REPLACE' : 'INSERT') . " INTO `{$table}` (";
        $keys = array_keys($data);
        $values = array_values($data);

        $query .= implode(", ", $keys);
        $query .= ") VALUES (";

        $array = [];

        foreach ($values as $value) {
            $array[] = $this->addParam($value);
        }

        $query .= implode(", ", $array) . ")";

        $this->sql = $query;
        return $this;
    }

    /**
     * SET.  $data is an array matched key => value. set(array("username" =>
     * "Caleb"))
     *
     * @param array $data The data
     *
     * @return self
     */
    public function set($data)
    {
        if (!is_array($data)) {
            return $this;
        }

        $query = "SET ";
        $array = [];
        foreach ($data as $key => $value) {
            if (is_numeric($value)) {
                $array[] = "`{$key}`={$this->addParam($value)}";
            } else {
                $array[] = "`{$key}`='{$this->addParam($value)}'";
            }
        }
        $query .= implode(", ", $array);
        $this->sql .= " " . $query;
        return $this;
    }

    /**
     * WHERE.  $fields and $values can either be strings or arrays based on how
     * many you need. $operators can be an array to add in <, >, etc.  Must
     * match the index for $fields and $values. where("username", "Caleb")
     * where(array("username", "password"), array("Caleb", "testing"))
     * where(array("username", "level"), array("Caleb", "10"), array("=", "<"))
     *
     * @param array|string $fields The fields
     * @param array|string $values The values
     * @param array|string $operators The operators
     *
     * @return self
     */
    public function where($fields, $values, $operators = '')
    {
        if (!is_array($fields) && !is_array($values)) {
            if ($fields == 'match') {
                $query = " WHERE MATCH('{$values}')";
            } else {
                $operator = (!empty($operators) && in_array($operators[0], $this->allowedOperators)) ? $operators[0] : '=';
                $query = " WHERE `{$fields}` {$operator} " . (is_numeric($values) ? "{$values}" : "{$this->addParam($values)}");
            }
        } else {
            $array = array_combine($fields, $values);
            $query = " WHERE ";
            $data = [];
            $counter = 0;
            foreach ($array as $key => $value) {
                if ($key == 'match') {
                    $data[] = "MATCH({$this->addParam($value)})";
                } else {
                    $operator = (!empty($operators) && !empty($operators[$counter]) && in_array($operators[$counter], $this->allowedOperators)) ? $operators[$counter] : '=';
                    $data[] = "`{$key}` {$operator} " . (is_numeric($value) ? "{$value}" : "{$this->addParam($value)}");
                }
                $counter++;
            }
            $query .= implode(" AND ", $data);
        }
        $this->sql .= $query;
        return $this;
    }

    /**
     * Order By: order_by("username", "asc")
     *
     * @param string $field The field
     * @param string $direction The direction
     *
     * @return self
     */
    public function order_by($field, $direction = 'asc')
    {
        if ($field) {
            $this->sql .= " ORDER BY `{$field}` " . strtoupper($direction);
        }
        return $this;
    }

    /**
     * Limit: limit(1) limit(1, 0)
     *
     * @param string $max The maximum
     * @param string $min The minimum
     *
     * @return self
     */
    public function limit($max, $min = '0')
    {
        if ($max) {
            $this->sql .= " LIMIT {$min},{$max}";
        }
        return $this;
    }

    /**
     * @param mixed $sql
     *
     * @return QueryBuilder
     */
    public function setSql($sql)
    {
        $this->sql = $sql;

        return $this;
    }

    /**
     * Adds a parameter.
     *
     * @param mixed $value The value
     *
     * @return string
     */
    private function addParam($value): string
    {
        if (is_array($value)) {
            $value = json_encode($value);
        }

        $name = 'p' . substr(md5($value), 0, 8);

        $this->params[$name] = $value;
        return ':' . $name;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $params
     *
     * @throws Exception
     * @return QueryBuilder
     */
    public function setParams(array $params): self
    {
        foreach ($params as $key => $param) {
            if (!is_string($key)) {
                throw new Exception(sprintf('Invalid Param Key `%s`', $key), 404);
            }
        }

        $this->params = $params;

        return $this;
    }
}
