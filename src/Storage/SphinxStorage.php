<?php
namespace ShopExpress\PdoCrud\Storage;

use Exception;
use PDO;
use PDOException;
use PDOStatement;
use ShopExpress\PdoCrud\Factory\StorageFactoryConfiguration;
use ShopExpress\PdoCrud\QueryBuilder;

class SphinxStorage implements StorageInterface
{
    protected $connection;

    protected $mapDatabase;

    /**
     * @param StorageFactoryConfiguration $configuration The configuration
     *
     * @throws Exception
     */
    public function __construct(StorageFactoryConfiguration $configuration)
    {
        try {
            $connectionString = sprintf(
                "mysql:host=%s;port=%s",
                $configuration->getHost(),
                $configuration->getPort()
            );

            $this->connection = new PDO($connectionString);
            $this->connection->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, "SET NAMES utf8");
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            throw new Exception('Подключение не удалось: ' . $e->getMessage());
        }
    }

    /**
     * @return PDO
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Map the structure of the MySQL db to an array
     *
     * @return array Returns array of db structure
     */
    public function mapDatabase()
    {
        if (!isset($this->mapDatabase)) {
            // Map db structure to array
            $tables_arr = [];

            foreach ($this->connection->query("SHOW TABLES")->fetchAll() as $table) {
                $tables_arr[$table['Index']] = [];
            }
            foreach ($tables_arr as $table_name => $val) {
                $tables_arr[$table_name]['index'] = 'id';
                foreach ($this->connection->query("DESCRIBE {$table_name}") as $field) {
                    $tables_arr[$table_name]['fields'][] = $field['Field'];
                }
            }
            $this->mapDatabase = $tables_arr;
        }
        return $this->mapDatabase;
    }

    /**
     * у SphinxQL нет execute(params)
     *
     * @param QueryBuilder $qb
     *
     * @return PDOStatement
     */
    public function query(QueryBuilder $qb)
    {
        $query = $qb->getSql();
        if($qb->getParams()){
            foreach ($qb->getParams() as $key => $param) {
                $query = str_replace(':' . ltrim($key, ':'), $this->quote($param), $query);
            }
        }

        return $this->connection->query($query, PDO::FETCH_ASSOC);
    }

    /**
     * Adds quotes around values when necessary.
     * Based on FuelPHP's quoting function.
     * @inheritdoc
     */
    public function quote($value)
    {
        if ($value === true) {
            return 1;
        } elseif ($value === false) {
            return 0;
        } elseif (is_int($value)) {
            return (int) $value;
        } elseif (is_float($value)) {
            // Convert to non-locale aware float to prevent possible commas
            return sprintf('%F', $value);
        } elseif (is_array($value)) {
            // Supports MVA attributes
            return '('.implode(',', $this->quoteArr($value)).')';
        }
        return $this->connection->quote($value);
    }

    /**
     * @param array $array
     * @return array
     */
    public function quoteArr(array $array = array())
    {
        $result = array();
        foreach ($array as $key => $item) {
            $result[$key] = $this->quote($item);
        }
        return $result;
    }

    /**
     * Gets the last insert identifier.
     *
     * @return boolean The last insert identifier.
     */
    public function getLastInsertId()
    {
        return false;
    }
}
