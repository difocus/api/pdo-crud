<?php
namespace ShopExpress\PdoCrud\Storage;

use Exception;
use PDO;
use PDOException;
use PDOStatement;
use ShopExpress\PdoCrud\Factory\StorageFactoryConfiguration;
use ShopExpress\PdoCrud\QueryBuilder;

class MysqlStorage implements StorageInterface
{
    protected $connection;
    protected $configuration;

    protected $mapDatabase;

    /**
     * @param StorageFactoryConfiguration $configuration The configuration
     *
     * @throws Exception
     */
    public function __construct(StorageFactoryConfiguration $configuration)
    {
        try {
            $connectionString = sprintf(
                "%s:dbname=%s;host=%s;port=%s",
                $configuration->getDriverName(),
                $configuration->getDb(),
                $configuration->getHost(),
                $configuration->getPort()
            );

            $this->connection = new PDO(
                $connectionString,
                $configuration->getLogin(),
                $configuration->getPassword(),
                [
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                ]
            );

            $this->configuration = $configuration;
        } catch (PDOException $e) {
            throw new Exception('Подключение не удалось: ' . $e->getMessage());
        }
    }

    /**
     * @return PDO
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * Map the structure of the MySQL db to an array
     *
     * @return array Returns array of db structure
     */
    public function mapDatabase()
    {
        if (!isset($this->mapDatabase)) {
            // Map db structure to array
            $tables_arr = [];

            foreach ($this->connection->query("SHOW TABLES FROM `" . $this->configuration->getDb() . '`') as $table) {
                if (isset($table["Tables_in_" . $this->configuration->getDb()])) {
                    $table_name = $table["Tables_in_" . $this->configuration->getDb()];
                    $tables_arr[$table_name] = [];
                }
            }
            foreach ($tables_arr as $table_name => $val) {
                foreach ($this->connection->query("SHOW COLUMNS FROM `{$table_name}`") as $field) {
                    $tables_arr[$table_name]['fields'][] = $field['Field'];
                    if (isset($field['Key']) && $field['Key'] === 'PRI') {
                        $tables_arr[$table_name]['index'] = $field['Field'];
                    }
                }
            }
            $this->mapDatabase = $tables_arr;
        }
        return $this->mapDatabase;
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return PDOStatement
     */
    public function query(QueryBuilder $qb)
    {
        $stmt = $this->connection->prepare($qb->getSql());
        $stmt->execute($qb->getParams());

        return $stmt;
    }

    /**
     * Gets the last insert identifier.
     *
     * @return int The last insert identifier.
     */
    public function getLastInsertId()
    {
        return $this->connection->lastInsertId();
    }
}
