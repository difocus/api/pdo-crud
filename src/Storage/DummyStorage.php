<?php

namespace ShopExpress\PdoCrud\Storage;


use ShopExpress\PdoCrud\Factory\StorageFactoryConfiguration;
use ShopExpress\PdoCrud\QueryBuilder;

class DummyStorage implements StorageInterface
{
    protected $configuration;

    /**
     * @param StorageFactoryConfiguration $configuration The configuration
     */
    public function __construct(StorageFactoryConfiguration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return mixed
     */
    public function getConnection()
    {
        return true;
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return bool
     */
    public function query(QueryBuilder $qb)
    {
        $s = $qb->getSql();
        $p = $qb->getParams();

        return true;
    }

    /**
     * Map the structure of the MySQL db to an array
     *
     * @return array Returns array of db structure
     */
    public function mapDatabase()
    {
        $tables_arr['shops']['fields'][] = ['id', 'field_1'];

        return $tables_arr;
    }

    /**
     * Gets the last insert identifier.
     *
     * @return int The last insert identifier.
     */
    public function getLastInsertId()
    {
        return false;
    }
}