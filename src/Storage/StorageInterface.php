<?php
namespace ShopExpress\PdoCrud\Storage;

use PDOStatement;
use ShopExpress\PdoCrud\Factory\StorageFactoryConfiguration;
use ShopExpress\PdoCrud\QueryBuilder;

interface StorageInterface
{
    /**
     * @param StorageFactoryConfiguration $configuration The configuration
     */
    public function __construct(StorageFactoryConfiguration $configuration);

    /**
     * @return mixed
     */
    public function getConnection();

    /**
     * @param QueryBuilder $qb
     *
     * @return PDOStatement
     */
    public function query(QueryBuilder $qb);
    
    /**
     * Map the structure of the MySQL db to an array
     *
     * @return array Returns array of db structure
     */
    public function mapDatabase();

    /**
     * Gets the last insert identifier.
     *
     * @return int The last insert identifier.
     */
    public function getLastInsertId();
}
